package homeworks.homework12;

import java.util.Arrays;
import java.util.Random;

/**
 * вывести самый часто встречаемый возраст людей
 * используем сортировку подсчетом
 * то есть создаем массив, длина которого равна
 * количеству возмжоных значений сортируемой характеристики
 * Найти максимум в массиве возрастов.
 */

public class MainApp {
    public static void main(String[] args) {
        Random random = new Random();
        String[] names = {"Name1", "Name2", "Name3", "Name4"};

        // массив из 100 объектных переменных
        Human[] humans = new Human[100];

        for (int i = 0; i < humans.length; i++) {
            Human human = new Human(names[random.nextInt(4)].toCharArray(), random.nextInt(Human.MAX_AGE));
            humans[i] = human;
        }

        //humans[58].age = -177;
        humans[58].setAge(-177);
        // например, в 37-м элементе
        // данного массива будет лежать 45
        // это значит, что возраст 37 встретился
        // 45 раз

        int[] ages = new int[Human.MAX_AGE];
        int currentAge = 0;

        for (int i = 0; i < humans.length; i++) {
            // получили возвраст человека
            // int currentAge = humans[i].age;
            currentAge = humans[i].getAge();
            ages[currentAge]++;
        }

        int maxRepeat = 0;
        int old = 0;
        for (int i = 0; i < ages.length; i++) {
            if (ages[i] > maxRepeat) {
                maxRepeat = ages[i];
                old = i;
            }
        }

        //Найти максимум в массиве возрастов и само часто повторяющийся.
        int max = 0;
        for (int i = 0; i < humans.length; i++) {
            System.out.println(Arrays.toString(humans[i].getName()) + " " + humans[i].getAge());
            if(humans[i].getAge() > max) {
                max = humans[i].getAge();
            }
        }
        System.out.println("Само часто повторяющийся возраст это: " + old + ", повторяется " + maxRepeat + " раз(а)");
        System.out.println("Самый максимальный возраст в списке: " + max);
    }
}