package homeworks.homework12;

public class Human {
    static final int MAX_AGE = 130;
    static final int MIN_AGE = 0;

    private char[] name;
    private int age;

    Human(char[] name, int age) {
        //this.name = name;
        //this.age = age;
        setName(name);
        setAge(age);
    }

    public void setName(char[] name) {
        this.name = name;
    }

    public char[] getName() {
        return name;
    }

    public void setAge(int age) {
        if (age >= MIN_AGE && age <= MAX_AGE) {
            this.age = age;
        } else {
            age = MIN_AGE;
        }
    }

    public int getAge() {
        return age;
    }

}
