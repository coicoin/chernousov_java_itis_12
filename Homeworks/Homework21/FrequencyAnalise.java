package homeworks.homework21;

import java.util.*;
import java.util.stream.Collectors;

public class FrequencyAnalise {
    private final int MAX_NUMBER_OF_CHARS = 10;
    private Scanner scanner = new Scanner(System.in);

    public void getCharacterFrequency() {
        double level;
        char[] input;
        System.out.println("Введите данные: ");
        input = scanner.nextLine().toCharArray();
        HashMap<Character, Integer> data = new HashMap<>();
        Integer count;
        for (char i : input) {
            count = data.get(i);
            data.put(i, count == null ? 1 : count + 1);
        }
        //.peek() Служит для того, чтобы передать элемент куда-нибудь, не разрывая при этом цепочку операторов.
        //.forEach() — терминальный оператор и после него стрим завершается?), либо для отладки.
        Map<Character, Integer> result = data.entrySet()
                .stream()
                .sorted(Map.Entry.<Character, Integer>comparingByValue().reversed())
                .limit(MAX_NUMBER_OF_CHARS)
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
        System.out.println("Количество одинаковых символов: " + result);

        // Obtain the entry with the min value:
        Map.Entry<Character, Integer> entryWithMinValue = Collections.min(
                result.entrySet(), Map.Entry.comparingByValue());
        System.out.println("Минимальный результат: " + entryWithMinValue);
        // Obtain the entry with the max value:
        Map.Entry<Character, Integer> entryWithMaxValue = Collections.max(
                result.entrySet(), Map.Entry.comparingByValue());
        System.out.println("Максимальный результат: " + entryWithMaxValue);

        // get graphical schema
        System.out.println("\n---===== ГРАФИЧЕСКИЙ АНАЛИЗ ПОДСЧЕТА СИМВОЛОВ: =====---");
        for (Map.Entry<Character, Integer> entry : result.entrySet()) {
            System.out.printf("%-2s", entry.getKey());
            //get level by percent
            level = ((double) entry.getValue()/entryWithMaxValue.getValue())*10;
            for (int i = 0; i < (int) level; i++) {
                System.out.printf("%2c", '#');
            }
            System.out.printf("%4d%n", entry.getValue());
        }

        System.out.println("\n---===== СТОЛБИКОВАЯ ДИАГРАММА ПОДСЧЕТА СИМВОЛОВ: =====---");
        //get symbol
        for (Map.Entry<Character, Integer> entry : result.entrySet()) {
            System.out.printf("%2d", entry.getValue());
        }
        System.out.println();
        //get level by percent
        //for (Map.Entry<Character, Integer> entry : result.entrySet()) {
            //level = ((double) entry.getValue()/entryWithMaxValue.getValue())*10;
            for (int i = 1; i <= 10; i++) {
                for (int k = 1; k <= result.size(); k++) {
                    //if (level >= i) {
                        System.out.printf("%2c", '#');
                        /*for (int j = 0; j < result.size() - k; j++) {
                            System.out.printf("%2c", ' ');
                        }*/
                    //}
                }
            System.out.println();
        }
        //get count
        System.out.println();
        for (Map.Entry<Character, Integer> entry : result.entrySet()) {
            System.out.printf("%2s", entry.getKey());
        }
    }
}
