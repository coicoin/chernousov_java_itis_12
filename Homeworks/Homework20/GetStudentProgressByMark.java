package homeworks.homework20;

import java.util.Scanner;

public class GetStudentProgressByMark {
    private final int MAX_WEEK = 18;
    private final int NUMBER_OF_MARKS_PER_WEEK = 5;
    private final int MIN_MARK = 1;
    private final int MAX_MARK = 9;
    private int currentMark;
    private int minConcat = 0;
    private int weekCounter = 0;
    private Scanner scanner = new Scanner(System.in);

    private void getMarkForWeek() {
        int min = 9;
        for (int i = 1; i <= MAX_WEEK; i++) {
            System.out.println("Week " + i);
            for (int j = 1; j <= NUMBER_OF_MARKS_PER_WEEK; j++) {
                currentMark = scanner.nextInt();
                if (currentMark >= MIN_MARK && currentMark <= MAX_MARK) {
                    System.out.print(currentMark + " ");
                    if (currentMark < min)
                        min = currentMark;
                } else if (currentMark == 42)
                    break;
                else {
                    System.out.print("\nWrong mark! Please, input mark in range from 1 to 9.");
                    j--;
                }
            }
            minConcat += min;
            System.out.println();
            if (currentMark == 42)
                break;
            minConcat *= 10;
            weekCounter = i;
        }
        if (minConcat > 0) {
            while (minConcat > 0) {
                min += minConcat % 10;
                min *= 10;
                minConcat = minConcat / 10;
            }
            minConcat = min/10;
        }

    }

    public void getWeeklyGraphicalProgress() {
        getMarkForWeek();
        int min = minConcat;
        if (weekCounter > 0) {
            for (int i = 1; i <= weekCounter; i++) {
                if (min > 9) {
                    min = min % 10;
                }
                if (min > 0 && min <= 9) {
                    StringBuilder level = new StringBuilder(" ");
                    for (int j = 1; j <= min; j++) {
                        level.append("=");
                    }
                    System.out.print("Week " + i + level.toString() + ">");
                }
                System.out.println();
                min = minConcat/10;
            }
        }
    }
}
