package homeworks.homework14;

import java.time.LocalTime;

public class TVProgram {
    private String program;

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public TVProgram(String program) {
        this.program = program;
    }

    @Override
    public String toString() {
        return "TVProgram{" +
                "program='" + program + '\'' +
                '}';
    }
}
