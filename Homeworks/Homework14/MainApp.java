package homeworks.homework14;

public class MainApp {
    public static void main(String[] args) {
        //for TNT
        TVProgram interns = new TVProgram("Интерны");
        TVProgram fizruk = new TVProgram("Физрук");
        TVProgram comedyClub = new TVProgram("Comedy Club");
        //for RUSSIA
        TVProgram spec = new TVProgram("Спецназ");
        TVProgram truckers = new TVProgram("Дальнобойщики");
        TVProgram bandPeter = new TVProgram("Бандитский Петербург");
        //for NTV
        TVProgram cops = new TVProgram("Менты");
        TVProgram news = new TVProgram("Новости");
        TVProgram shef = new TVProgram("Шеф");

        Channel tnt = new Channel("ТНТ", new TVProgram[]{interns, fizruk, comedyClub});
        Channel russia = new Channel("Россия", new TVProgram[]{spec, truckers, bandPeter});
        Channel ntv = new Channel("НТВ", new TVProgram[]{cops, news, shef});

        Televisor samsung = new Televisor(new Channel[]{tnt, russia, ntv});
        System.out.println(samsung.toString());
        System.out.println();
        samsung.switchOnTV();
    }
}