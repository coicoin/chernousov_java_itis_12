package homeworks.homework14;

import java.util.Arrays;
import java.util.Scanner;

public class Televisor {
    private Scanner scanner = new Scanner(System.in);
    private Channel[] channels;
    private Channel channel;

    //for TNT
    /*private TVProgram interns = new TVProgram("Интерны");
    private TVProgram fizruk = new TVProgram("Физрук");
    private TVProgram comedyClub = new TVProgram("Comedy Club");
    //for RUSSIA
    private TVProgram spec = new TVProgram("Спецназ");
    private TVProgram truckers = new TVProgram("Дальнобойщики");
    private TVProgram bandPeter = new TVProgram("Бандитский Петербург");
    //for NTV
    private TVProgram cops = new TVProgram("Менты");
    private TVProgram news = new TVProgram("Новости");
    private TVProgram shef = new TVProgram("Шеф");

    private Channel tnt = new Channel("ТНТ", new TVProgram[]{interns, fizruk, comedyClub});
    private Channel russia = new Channel("Россия", new TVProgram[]{spec, truckers, bandPeter});
    private Channel ntv = new Channel("НТВ", new TVProgram[]{cops, news, shef});
    */

    public Televisor(Channel[] channels) {
        this.channels = channels;
    }
    //включает телевизор, и выбор меню
    public void switchOnTV() {
        String choice = null;
        while (choice == null) {
            System.out.println("Нажмите любую кнопку, чтобы включить телевизор. ");
            choice = scanner.next();
        }

        System.out.println("МЕНЮ");
        System.out.printf("%2d. %s%n", 1, "Выбрать канал");
        System.out.printf("%2d. %s%n", 2, "YouTube");
        System.out.printf("%2d. %s%n", 3, "MEGOGO");
        System.out.printf("%2d. %s%n", 4, "WEB браузер");

        choice = scanner.next();
            switch (choice) {
                case "exit":
                    System.out.println("Телевизор выключен... ");
                    return;
                case "1":
                    chooseChannel();
                    break;
                case "2":
                    System.out.println("Открывается YouTube... ");
                    break;
                case "3":
                    System.out.println("Вы вошли на MEGOGO. Приятного просмотра");
                    break;
                case "4":
                    System.out.println("Введите запрос в поле поиска... ");
                    break;
                default:
                    System.out.println("Команды не найдено... ");
                    break;
        }
    }

    //выбирает канал
    public void chooseChannel() {
        String choice;

        System.out.println("Выберите канал: ");
        System.out.printf("%2d. %s%n", 1, channels[0].getChannel());
        System.out.printf("%2d. %s%n", 2, channels[1].getChannel());
        System.out.printf("%2d. %s%n", 3, channels[2].getChannel());
        System.out.printf("%2d. %s%n", 4, "ТВ3");
        System.out.printf("%2d. %s%n", 5, "MTV");
        System.out.printf("%2d. %s%n", 6, "СТС");
        System.out.printf("%2d. %s%n", 7, "MusicBox");
            choice = scanner.next();
            switch (choice) {
                case "exit":
                    System.out.println("Телевизор выключен... ");
                    switchOnTV();
                    break;
                case "1":
                    System.out.println(channels[0].getChannel());
                    channel = channels[0];
                    channel.showProgram(channels[0]);
                    break;
                case "2":
                    System.out.println(channels[1].getChannel());
                    channel = channels[1];
                    channel.showProgram(channels[1]);
                    break;
                case "3":
                    System.out.println(channels[2].getChannel());
                    channel = channels[2];
                    channel.showProgram(channels[2]);
                    break;
                case "4":
                    System.out.println("Введите запрос в поле поиска... ");
                    break;
                case "5":
                case "6":
                case "7":
                    System.out.println("Нет сигнала... ");
                    break;
                default:
                    System.out.println("Команды не найдено... ");
                    break;
            }
    }

    @Override
    public String toString() {
        return "Televisor{" +
                "scanner=" + scanner +
                ", channels=" + Arrays.toString(channels) +
                '}';
    }
}
