package homeworks.homework14;

import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;

public class Channel {
    private String channel;
    private TVProgram[] programs;
    private LocalTime time = LocalTime.now(ZoneId.of("Europe/Moscow"));

    public Channel(String channel, TVProgram[] programs) {
        this.channel = channel;
        this.programs = programs;
    }

    public Channel(String channel) {
        this.channel = channel;
    }

    public String getChannel() {
        return channel;
    }

    public TVProgram[] getPrograms() {
        return programs;
    }

    public void showProgram(Channel channel) {
        if (time.getHour() >= 10 && time.getHour() <= 12) {
            System.out.printf("Сейчас идет %s, текущее время: %s:%s%n", channel.programs[0].getProgram(),
                    time.getHour(), time.getMinute());
        } else if (time.getHour() > 12 && time.getHour() <= 15) {
            System.out.printf("Сейчас идет %s, текущее время: %s:%s%n", channel.programs[1].getProgram(),
                    time.getHour(), time.getMinute());
        } else if (time.getHour() > 15 && time.getHour() <= 22) {
            System.out.printf("Сейчас идет %s, текущее время: %s:%s%n", channel.programs[2].getProgram(),
                    time.getHour(), time.getMinute());
        } else {
            System.out.println("Сейчас ничего не идет по телевизору");
        }
    }

    @Override
    public String toString() {
        return "Channel{" +
                "channel='" + channel + '\'' +
                ", programs=" + Arrays.toString(programs) +
                ", time=" + time +
                '}';
    }
}
