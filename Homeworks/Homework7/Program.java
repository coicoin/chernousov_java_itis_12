package homeworks.homework7;

/**
 *
 * 1. Написать функцию, возвращающую сумму цифр определенного числа.
 *
 * 2. Написать функцию, возвращающую зеркальное представление числа.
 *
 * 3. Написать функцию, которая возвращает сумму чисел, которые находятся в диапазоне от a до b.
 */

public class Program {
    public static void main(String[] args) {

        //задание 1. Сумма цифр числа.
        System.out.println("\nЗадание 1. Сумма цифр числа.");
        System.out.println(sum(22));
        System.out.println(sum(235));
        System.out.println(sum(2002000));

        //Задание 2. Зеркальное представление числа
        System.out.println("\nЗадание 2. Зеркальное представление числа");
        System.out.print(mirrorNumber(12345));
        System.out.println();

        //Задание 3. Сумма чисел в диапазоне от a до b
        System.out.println("\nЗадание 3. Сумма чисел в диапазоне от a до b");
        System.out.println(sumInRange(5, 10));

    }

    /**
     *
     * @param x - принимает число как аргумент, цифры которого складываются.
     * @return - возвращает сумму цифр числа.
     */
    public static int sum(int x) {
        int result = 0;
        while (x > 0) {
            result += x%10;
            x = x/10;
        }
        return result;
    }

    /**
     *
     * @param mirrorNum - принимает число как аргумент, которое зеркалируется.
     * @return - возвращает зеркалируемое число.
     */
    public static int mirrorNumber(int mirrorNum) {
        int result = mirrorNum/10;
        while (mirrorNum > 0) {
            result = result * 10;
            result += mirrorNum%10;
            mirrorNum = mirrorNum/10;
        }
        return result;
    }

    /**
     *
     * @param a - принимает стартовое значение, если a < b. Или наоборот конечное
     * @param b - принимает конечное значение, если a < b. Или наоброт стартовое
     * @return - возвращает сумму чисел в диапозоне от a до b
     */
    public static int sumInRange(int a, int b) {
        int result = 0;
        if (a < b) {
            for (int i = a; i <= b; i++) {
                result += i;
            }
        } else {
            for (int i = a; i >= b; i--) {
                result += i;
            }
        }
        return result;
    }
}
