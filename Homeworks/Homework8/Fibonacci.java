﻿package homeworks.homework8;

public class Fibonacci {
    public static void main(String[] args) {
        int x = fibonacci(10);
        System.out.println("Последний элемент: " + x);
    }

    public static int fibonacci(int n) {
        //int result;
        //без условия выбрасывается исключение StackOverflowError
        if (n == 0) {
            return 0;
        }

        if (n == 1) {
            return 1;
        }

        return fibonacci(n - 2) + fibonacci(n - 1);
    }
}