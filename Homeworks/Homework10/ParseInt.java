package homeworks.homework10;

/**
 * Реализовать функцию:
 *
 * public static int parseInt(char number[]) {
 *
 * }
 * char n[] = {'3', '2', '4'};
 * int x = parseInt(n); // x = 324
 */

public class ParseInt {
    public static void main(String[] args) {
        System.out.println("Преобразование в int с помощью метода getNumericValue");
        char n[] = {'3', '2', '4'};
        int x = parseInt(n); //x = 324
        System.out.println(x + "\n");

        System.out.println("unicode метод, HTML-код &#48 в unicode равен цифре 0. А значит 51 == 3, 50 == 2, 52 == 4");
        int y = parseInteger(n); //y = 324
        System.out.println(y);
    }

    //Получение с помощью метода getNumericValue
    public static int parseInt(char number[]) {
        int parsed = 0;
        for (int i = 0; i < number.length; i++) {
            parsed = parsed * 10;
            parsed += Character.getNumericValue(number[i]);
        }
        return parsed;
    }

    //unicode метод, HTML-код &#48 в unicode равен цифре 0. А значит 51 == 3, 50 == 2, 52 == 4
    public static int parseInteger(char number[]) {
        int parsed = 0;
        for (int i = 0; i < number.length; i++) {
            parsed = parsed * 10;
            parsed += (number[i] - 48);
        }
        return parsed;
    }

}