package ru.itdrive.threads.pool;

public class Main {
    public static void main(String[] args) {
        ThreadPool threadPool = new ThreadPool(5);

        threadPool.submit(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println(Thread.currentThread().getName() + " А");
            }
        });

        threadPool.submit(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println(Thread.currentThread().getName() + " B");
            }
        });
    }
}
