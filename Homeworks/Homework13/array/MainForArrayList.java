package homeworks.homework13.array;

public class MainForArrayList {

	public static void main(String[] args) {
		ArrayList list = new ArrayList();
		list.add(10);
		list.add(5);
		list.add(7);
		list.add(10);
		list.add(5);
		list.add(7);
		list.add(10);
		list.add(5);
		list.add(7);
		list.add(10);
		list.add(5);
		list.add(7);
		list.addToBegin(77);

		System.out.println("Вывод всех элементов: ");
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}

		//размер
		System.out.println("Размер: " + list.size());

		//проверка наличия элемента
		System.out.println("Проверка содержит ли элемент: ");
		System.out.println(list.contains(5));
		System.out.println(list.contains(1));

		//удаление по индексу
		System.out.println("Удаление по индексу: ");
		list.removeByIndex(0);
		System.out.println(list.get(0));
		System.out.println("Размер: " + list.size());
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}

		//удаление по элементу
		System.out.println("Удаление по элементу: ");
		list.remove(5);
		System.out.println("Размер: " + list.size());
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}
	}
}
