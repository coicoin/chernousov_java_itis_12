package homeworks.homework13.array;

public class ArrayList {
    private static final int DEFAULT_ARRAY_SIZE = 10;
    private static int newArraySize = DEFAULT_ARRAY_SIZE;

    private int[] elements;
    private int[] newElements;
    private int count;

    public ArrayList() {
        this.elements = new int[DEFAULT_ARRAY_SIZE];
        this.count = 0;
    }

    public void add(int element) {
        if (count < DEFAULT_ARRAY_SIZE || count < newArraySize) {
            this.elements[count] = element;
            this.count++;
        } else {
            newArraySize = (int) (count * 1.5);
            newElements = new int [newArraySize];
            for (int i = 0; i < elements.length; i++) {
                newElements[i] = elements[i];
            }
            elements = newElements;
            this.count++;
        }
    }

    public int get(int index) {
        if (index >= 0 && index < count) {
            return elements[index];
        } else {
            System.err.println("Неверный индекс");
            return -1;
        }
    }

    public void addToBegin(int element) {
        if (count < DEFAULT_ARRAY_SIZE || count < newArraySize) {
            for (int i = count; i > 0; i--) {
                elements[i] = elements[i - 1];
            }
            this.elements[0] = element;
            this.count++;
        } else {
            System.err.println("Нет места");
        }
    }

    public void remove(int element) {
        // TODO: реализовать
        int i, j;
        for (i = 0; i < elements.length; i++) {
            /*if(elements[i] == element) {
                System.out.println("Число " + elements[i] + " по индексу " + i + " удалено");
                elements[i] = -1;
                count--;
            }*/
            if (elements[i] == element) {
                System.out.println("Элемент " + elements[i] + " по индексу " + i + " удален");
                break;
            }
        }
        //сдвиг последующих элементов
        for (j = i; j < count; j++) {
            elements[j] = elements[j + 1];
        }
        count--;
    }

    public void removeByIndex(int index) {
        // TODO: удалить по индексу
        int j;
        if (index <= count) {
            System.out.println("Число " + elements[index] + " по индексу " + index + " удалено");
            //сдвиг последующих элементов
            for (j = index; j < count; j++) {
                elements[j] = elements[j + 1];
            }
        }
        count--;
    }

    public boolean contains(int element) {
        // TODO: реализовать
        for (int i = 0; i < elements.length; i++) {
            if(elements[i] == element) {
                return true;
            }
        }
        return false;
    }

    public int size() {
        return count;
    }
}
