package homeworks.homework13.nodes;

public class Node {
    private int value;
    //Потому что Node это указатель на следующий элемент, а следующий элемент - это такой же узел, как и этот
    private Node next;

    public Node(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }
}
