package homeworks.homework13.nodes;

public class Main {
    public static void main(String[] args) {
        Node a = new Node(3);
        Node b = new Node(7);
        Node c = new Node(8);
        Node d = new Node(9);

        a.setNext(b);
        b.setNext(c);
        c.setNext(d);

        Node current = a;

        while (current != null) {
            System.out.println(current.getValue());
            current = current.getNext();
        }
    }
}
