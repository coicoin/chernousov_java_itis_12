package homeworks.homework13.linked;

public class MainForLinkedList {
    public static void main(String[] args) {
        LinkedList list = new LinkedList();
        list.add(3);
        list.add(7);
        list.add(8);
        list.add(9);
        list.add(5);

//        System.out.println(list.get(0));
//        System.out.println(list.get(1));
//        System.out.println(list.get(2));
//        System.out.println(list.get(3));

        System.out.println("Linked list содержит элемент 3: " + list.contains(3));
        System.out.println("Linked list содержит элемент 1: " + list.contains(1));
        System.out.println("Linked list содержит элемент 5: " + list.contains(5));
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        list.remove(3);
        list.removeByIndex(2);

        LinkedListIterator iterator = new LinkedListIterator(list);
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
