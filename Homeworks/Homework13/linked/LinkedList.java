package homeworks.homework13.linked;

import homeworks.homework13.nodes.Node;

public class LinkedList {
    Node first;
    private Node last;

    private int count;

    public LinkedList() {

    }

    public void add(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
//            Node current = first;
//            while (current.getNext() != null) {
//                current = current.getNext();
//            }
//            current.setNext(newNode);
            last.setNext(newNode);
            last = newNode;
        }
        count++;
    }

    public int get(int index) {
        if (index >= 0 && index < count) {
            Node current = first;
            for (int i = 1; i <= index; i++) {
                current = current.getNext();
            }
            return current.getValue();
        } else {
            System.out.println("Нет такого элемента");
            return -1;
        }
    }

    public void addToBegin(int element) {
        // TODO: реализовать
        Node newNode = new Node(element);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
//            Node current = first;
//            while (current.getNext() != null) {
//                current = current.getNext();
//            }
//            current.setNext(newNode);
            last.setNext(newNode);
            last = newNode;
        }
        count++;
    }

    public void remove(int element) {
        // TODO: реализовать
        Node current = first;
        for (int i = 1; i <= count; i++) {
            if (current.getValue() != element) {
                current = current.getNext();
            } else if (current.getValue() == element) {
                current.setNext(current.getNext().getNext());
                break;
            } else {
                System.out.println("Элемент не найден.");
            }
        }

}

    public void removeByIndex(int index) {
        // TODO: удалить по индексу
        if (index >= 0 && index < count) {
            Node current = first;
            for (int i = 1; i <= count; i++) {
                if (i < index) {
                    current.setNext(current.getNext().getNext());
                    break;
                } else {
                    current = current.getNext();
                }
            }
        } else {
            System.out.println("Неверный индекс.");
        }
    }

    public boolean contains(int element) {
        // TODO: реализовать
        if (count > 0) {
            Node current = first;
            if (current.getValue() != element) {
                for (int i = 1; i < count; i++) {
                    current = current.getNext();
                    if (current.getValue() == element) {
                        return true;
                    }
                } return false;
            }
        } return true;
    }

    public int size() {
        return count;
    }
}
