﻿package homeworks.homework4;

import java.util.Scanner;

/**
 * Реализовать программу, которая для последовательности чисел, оканчивающихся -1,
 * выводит сумму чисел, произведение цифр которых делится на 3.
 * Разрешено использовать массивы
 */

public class SumAllDevidedThree {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

                                                        //С П О С О Б  1
        //Каждое число анализируется, если произведение цифр делится на 3, то число проходит в следующий этап и прибавляется.
        System.out.println("Способ 1: Обработка массива. Чтобы завершить ввод, введите -1.");
        int mult = 0; //произведение цифр числа
        int res = 0; //итоговая сумма чисел, произведение цифр которых делится на 3
        int[] arr = new int[0];

        //ввод массива, алгоритм рассширения массива
        System.out.println("Введите массив чисел: ");
        int currentNumber = scanner.nextInt();
        while (currentNumber != -1) {
            int[] increaseArr = new int[arr.length + 1];
            for (int j = 0; j < arr.length; j++) {
                increaseArr[j] = arr[j];
            }
            arr = increaseArr;
            arr[arr.length - 1] = currentNumber;

            //без данного шага уходит в бесконечный цикл ввода.
            currentNumber = scanner.nextInt();
        }

        //вывод всего массива
        System.out.println("Введенный массив: ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }

        int[] newArr = new int[arr.length];
        //вывод массива после произведения цифр в числе
        System.out.println("\nМассив после цикла произведения цифр в числе: ");
        for (int i = 0; i < arr.length; i++) {
            //проверка если число больше 10 или если число содержит 0
            if (arr[i] > 10) {
                mult = arr[i]%10 * (arr[i]/10);
            } else if (arr[i]%10 == 0) {
                mult = 0;
            } else {
                mult = arr[i];
            }
            System.out.print(mult + " ");
            //проверка деления на 3 и копирование в новый массив
            if (mult%3 == 0 && mult != 0) {
                newArr[i] = arr[i];
            }
        }

        //итого, сумма отобранных чисел
        System.out.println("\nМассив чисел, произведение цифр которых делится на 3: ");
        for (int i = 0; i < newArr.length; i++) {
            if (newArr[i] == 0) {
                continue;
            } else {
                System.out.print(newArr[i] + " ");
                res += newArr[i];
            }
        }
        System.out.println("\nИтоговая сумма чисел произведение цифр которых делится на 3: " + res);

                                                    //С П О С О Б  2
        //Вводятся числа, и если произведение цифр числа делится на 3, то числа прибавляются.
        System.out.println("Способ 2. Обработка примитивного типа int.");
        System.out.println("Введите число: ");
        Scanner scanner1 = new Scanner(System.in);
        int number = 0;
        int multInt = 0;
        int resultInt = 0;
        while (number != -1) {
            number = scanner1.nextInt();
            if (number != 0) {
                //resultInt = resultInt * 10;

                if (number > 10) {
                    multInt = number%10 * (number/10);
                } else if (number%10 == 0) {
                    multInt = 0;
                } else {
                    multInt = number;
                }
            }

            if (multInt%3 == 0 && multInt != 0) {
                System.out.println("Вы ввели число, произведение цифр которого делится на 3 и равно: " + multInt);
                resultInt += number;
            }
        }
        System.out.println("Итоговая сумма чисел произведение цифр которых делится на 3: " + resultInt);
    }
}