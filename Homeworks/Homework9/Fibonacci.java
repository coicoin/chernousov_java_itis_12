package homeworks.homework9;

    public class Fibonacci {
        public static void main(String[] args) {
            int x = fibonacci(10);
            System.out.println("Последний элемент: " + x);
        }

        public static int fibonacci(int n) {
            //int result;
            //без условия выбрасывается исключение StackOverflowError
            if (n == 0) {
                return 0;
            }

            if (n == 1) {
                return 1;
            }

            int m = n - 1;
            return fibonacci(n - 3);
        }
    }