﻿package homeworks.homework11;

import java.util.Scanner;

/**
 * На вход подается строка, состоящая из символов английского алфавита (символы - буквы (большие и маленькие), цифры, могут быть пробелы).
 *
 * Вывести на экран БУКВУ (G и g - одна и та же), которая встречается в тексте чаще всего.
 *
 * Если таких букв несколько - вывести их в алфавитном порядке.
 *
 * Ввод:
 * Hhello, how are you?
 * Вывод:
 * h - 3
 * o - 3
 */

public class EqualLetterCount {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите данные: ");
        String string = scanner.nextLine();
        countLetter(string);
    }

    public static void countLetter(String input) {
        int[] arr = new int[255];
        int maxIndex = 0;
        int max = 0;
        //преобразует в сторчные буквы
        input = input.toLowerCase();
        //выполняет подсчет элементов
        for (int i = 0; i < input.length(); i++) {
            arr[input.charAt(i)]++;
        }

        //вывод количества одинаковых элементов
        for (int i = 0; i < arr.length; i++) {
            //хотел бы вывести вместо ' ' слово "пробел"
            /* if (i == 32) {
                return "пробел";
            }*/
            if (arr[i] > 0) {
                System.out.println((char) i + " встречается " + arr[i] + " раз");
            }
        }

        //поиск максимального количества повторов
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        
        //выводит часто повторяющиеся элементы
        for (int i = 0; i < arr.length; i++) {
            if (max == arr[i]) {
                System.out.println("Само часто повторяется - " + (char) i + ", " + max + " раз(а)");
            }
        }
    }
}